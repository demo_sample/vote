import React, { Component } from 'react'

import VoteService from "components/vote/VoteService";
var Moment = require('moment-timezone');
var _ = require('lodash');

class VoteTable extends Component {
    
    constructor(props) {
        super(props) 
        this.state = {
            voteCampagins: [],
            userVotes: [],
            errorMessage: undefined
        };
    }
    
    getRemoteVoteDetail(){
        // refresh vote detail after mount
        VoteService.VoteDetail(this.handleVoteDetailResponse, this.handleVoteDetailError)
    }

    getUserVoteDetail(){
        // refresh user vote detail if inpurt hkid
        if(this.props.hkID !== ""){
            VoteService.PostUserVoteDetail(this.props.hkID, this.handleUserVoteDetailResponse, this.handleUserVoteDetailError)
        }else{
            this.setState({userVotes: [], refresh: false}) ;
            this.props.fetchVoteCampains(this.state.voteCampagins);
        }
    }

    // handle get vote detail
    handleVoteDetailResponse = (response) => {
        if(response) { 
          this.setState({voteCampagins : response.data.voteCampagins}); 
          this.props.fetchVoteCampains(this.state.voteCampagins);
          this.getUserVoteDetail();
        }
    };
  
    handleVoteDetailError = (err) => {
        if (err.response && err.response.status === 400)
            this.setState({errorMessage: err.response.data.message});
        else
            this.setState({errorMessage: "unknow Error"});

        this.getUserVoteDetail();
    };

    // handle get user vote detail
    handleUserVoteDetailResponse = (response) => {
        if(response) { 
          this.setState({userVotes : response.data.userVotes, refresh : false}); 
          this.props.fetchVoteCampains(this.state.voteCampagins);
        }
    };
    
    handleUserVoteDetailError = (err) => {
        if (err.response && err.response.status === 400)
            this.setState({errorMessage: err.response.data.message, refresh : false});
        else
            this.setState({errorMessage: "unknow Error", refresh : false});
    };
  
    componentDidMount(){
       this.getRemoteVoteDetail();
    };

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.refresh !== this.props.refresh && this.props.refresh) {
            this.setState({refresh: true});
            this.getRemoteVoteDetail();
        }
    }
      
    // render vote table
    renderVoteCampagin() {
        return this.state.voteCampagins.map((voteCampagin, index) => {
            var { no, title, startDate, endDate} = voteCampagin;
            // change to hk time zone
            var format = 'YYYY/MM/DD HH:mm:ss';
            startDate = Moment(startDate).tz("Asia/Hong_Kong").format(format);
            endDate = Moment(endDate).tz("Asia/Hong_Kong").format(format);
            
            return ([
                <tr key={no}>
                    <td width="50">No : {no}</td>
                    <td>{title} <br /> ({startDate} ~ {endDate})</td>
                </tr>,
                this.renderVoteOption(voteCampagin)
                
            ])            
        });
     }

    renderVoteOption(voteCampagin) {
        var userVoteOption = _.filter(this.state.userVotes, (userVote) => {
            return (voteCampagin._id === userVote.voteCampagin) ? true : false;
        })
        
        userVoteOption = _.map(userVoteOption, (userVote) => {return userVote.voteOptionValue});

        var voteOptions = voteCampagin.voteOptions;
        return _.map(voteOptions, (voteOption, index) => {
            var match = voteOption.value.toString() === userVoteOption.toString();
            return (
                <tr key={index}>
                    <td></td>
                    <td>
                        {voteOption.value} {match ? '(You voted)' : ''} 
                        <br />
                        <font size="2">Total votes: {voteOption.voteCount}</font>
                    </td>
                </tr>
           )
        })
    }
  
    render() {
        // <VoteMain func = {this.getVoteCampagin } />;
        return ( 
            
            this.state.voteCampagins.length > 0 ? (
                <div align="center">
                    <table id="voteTable" cellPadding="5">
                        <tbody>
                            {this.renderVoteCampagin()}
                        </tbody>
                    </table>
                </div>
            ) : null
            
        );
    }
  
}

export default VoteTable ;
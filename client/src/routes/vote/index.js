import React, {Component} from 'react';
// import {Route} from "react-router-dom";
import VoteTable from "routes/vote/VoteTable";
import VoteForm from "routes/vote/VoteForm";
import VoteService from "components/vote/VoteService";

class Vote extends Component {

  constructor(props) {
    super(props);
    this.state = {
      hkID: "",
      campaginID: "",
      voteOption: "",
      errorMessage:"",
      validCampaginNo: true
    };
  }
  
  // callback in VoteTable
  fetchVoteCampains = (voteCampagins) => {
    this.setState({voteCampagins : voteCampagins,refreshTable : false});
    this.setState({campaginID: voteCampagins[0].no});
  }

  // callback in VoteForm
  updateHKID = (input) => {
    this.setState({errorMessage : "", hkID: input, validHKID: true});
  }
 
  updateCampaginID = (input) => {
    var campaginIDs = this.state.voteCampagins.map(function(voteCampagin) {
      return voteCampagin.no;});
    this.setState({campaginID: input});
    try{
      if(campaginIDs.includes(parseInt(input))){
        this.setState({errorMessage : "", validCampaginNo: true});
      } else {
        this.setState({errorMessage : "campagin No not found", validCampaginNo: false});
      }
    } catch(err){
      this.setState({errorMessage : "campaginID incorrect", validCampaginNo: false});
    }
  }

  updateVoteOption = (input) => {
    this.setState({errorMessage : ""})
    this.setState({voteOption: input , validVoteOption: true});
  }
  
  handleFormSubmit = (event) => {
    this.updateCampaginID(this.state.campaginID);
    if (!(this.state.validHKID && 
          this.state.validCampaginNo && 
          this.state.validVoteOption)) {
      return;
    }
    
    VoteService.PostUserVote(
      this.state.hkID,
      this.state.campaginID,
      this.state.voteOption,
      this.handleUserVoteResponse, 
      this.handleUserVoteError)
  }

  // handle user vote
  handleUserVoteResponse = (response) => {
    if(response) { 
      this.setState({refreshTable : true});
    }
  };

  handleUserVoteError = (err) => {
    if (err.response && err.response.status === 400){
        this.setState({errorMessage: err.response.data.Message});
    } else
        this.setState({errorMessage: "unknown Error"});

      this.setState({refreshTable : true});
  };

  render(){
    return (
      <div>
        vote main page
        {/* <Route component={VoteTable} />   */}

        <VoteTable 
          fetchVoteCampains={this.fetchVoteCampains}
          hkID={this.state.hkID}
          refresh={this.state.refreshTable}
          />
        {/* <Route component={VoteForm} />   */}
        <VoteForm 
          updateHKID={this.updateHKID}
          updateCampaginID={this.updateCampaginID}
          updateVoteOption={this.updateVoteOption}
          handleFormSubmit={this.handleFormSubmit}
          voteCampagins={this.state.voteCampagins}
          defaultCampaginID={this.state.campaginID}
          errorMessage={this.state.errorMessage}
           />
      </div>
    );
  }
  
}

export default Vote;

import React, { Component } from 'react'

class VoteForm extends Component {

    constructor(props) {
        super(props) 
        this.state = {
            errorMessage: undefined
        };
    }
    
    renderCampaginSelection(){
        if(this.props.voteCampagins ){
            return (
                <select name="campaginNo"
                    value={this.props.defaultCampaginID} 
                    onChange={(e) => this.props.updateCampaginID(e.target.value)}>

                    {this.props.voteCampagins.map((voteCampagin, index) => {
                        var optionKey = "option" + voteCampagin.no;
                            return (
                                <option key={optionKey} value={voteCampagin.no}>{voteCampagin.no}</option>
                            ) ;
                    })}
                </select>
            )
        }
    }
    
    render() {
        return  (
            <div>
                <br />
                    <table align="center">
                        <tbody>
                            <tr>
                                <td align="right"> HKID :</td>
                                <td>
                                    <input type="text" name="HKID" size="50" onChange={(e) => this.props.updateHKID(e.target.value)} />
                                    <br />
                                    <div><font size="1">input HKD, e.g. T712534(2)</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Campagin no :</td>
                                <td>
                                    {this.renderCampaginSelection()}
                                    <br />
                                    <div><font size="1">(e.g. 1 for voting "Who is the best NBA player in the history")</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">Vote : </td>
                                <td>
                                    <input type="text" name="voteOption" size="50" onChange={(e) => this.props.updateVoteOption(e.target.value)} />
                                    <br />
                                    <div><font size="1">(e.g. Carrie Lam)</font></div>
                                </td>

                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <button type="button" onClick={(e) => this.props.handleFormSubmit(e.target.value)}>Vote</button>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <div><font color="red">{this.props.errorMessage}</font></div>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan="2">
                                    <div align="left">
                                        Remark: <br />
                                        1) if choice is not in vote option, new option will be added and voted after vote<br />
                                        2) your vote status will display after input HKID and press "Vote" <br />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
            </div>
        ) ;
    }
  
}

export default VoteForm ;
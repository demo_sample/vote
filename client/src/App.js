import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import './App.css';

// routes
import vote from "./routes/vote/index";

// asio config
const axios = require('axios');
axios.defaults.baseURL = process.env.REACT_APP_API_HOST;

class App extends Component {
  render(){
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={vote} />
          {/* <Route exact path="/test" component={test} /> */}
        </Switch>
      </div>
    );
  } 
}

export default App;

const axios = require('axios');

class VoteServices {
  
  static VoteDetail(successCallback, errorCallback) {
    axios.get('/v1/voteDetail').then(successCallback).catch(errorCallback);
  }

  static PostUserVoteDetail(hkID, successCallback, errorCallback) {
    var sendData = {
      "hkID" : hkID,
    }
    
    axios.post('/v1/userVoteDetail', sendData).then(successCallback).catch(errorCallback);
  }

  static PostUserVote(hkID, voteCampaginNo, voteOptionValue, successCallback, errorCallback) {

    var sendData = {
      "hkID" : hkID,
      "voteCampaginNo" : parseInt(voteCampaginNo),
      "voteOptionValue" : voteOptionValue,
    }
    
    axios.post('/v1/vote', sendData).then(successCallback).catch(errorCallback);
  }

}

export default VoteServices;
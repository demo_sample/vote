'use strict';
 
//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var userVoteSchema = new Schema({
  hkid              : String,
  voteCampagin    : { type: Schema.Types.ObjectId, ref: 'VoteCampagin' },
  voteOption      : { type: Schema.Types.ObjectId, ref: 'VoteOption' },
});

module.exports.MongooseModel = mongoose.model('UserVote', userVoteSchema );

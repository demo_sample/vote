'use strict';

//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var voteOptionSchema = new Schema({
  voteCampagin      : { type: Schema.Types.ObjectId, ref: 'VoteCampagin' },
  value             : String,
  voteCount         : Number,
});

module.exports.MongooseModel = mongoose.model('VoteOption', voteOptionSchema );

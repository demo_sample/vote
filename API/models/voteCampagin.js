'use strict';

//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var voteCampaginSchema = new Schema({
  no                : Number,
  title             : String,
  startDate         : Date,
  endDate           : Date,
});

module.exports.MongooseModel = mongoose.model('VoteCampagin', voteCampaginSchema);

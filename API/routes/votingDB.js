
'use strict';
 
const Router = require('express');
const router = new Router();

var connection = require('../controllers/database/database');
var voteCampaginController = require('../controllers/votingCampagin');

router.get('/', (req, res, next) => {
    res.send("vote db " + connection.connectHost);
});

router.get('/resetVoteCampagin', voteCampaginController.resetAllCampagin);
    
  
module.exports = router;

'use strict';
 
const Router = require('express');
const router = new Router();

var validate = require('express-jsonschema').validate;
var userVoteController = require('../controllers/userVote');
var userVoteInfoController = require('../controllers/userVoteInfo');
var voteCampaginController = require('../controllers/votingCampagin');

router.post('/vote', validate({body: userVoteController.RequestJSONSchema}), userVoteController.voting);
router.post('/userVoteDetail', validate({body: userVoteInfoController.RequestJSONSchema}), userVoteInfoController.userVoteInfo);

router.get('/voteDetail', voteCampaginController.GetCampaginDetail);

module.exports = router;
'use strict';

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';
const API_VERSION = 'v1';
// Variables
var express = require('express');
var helmet = require('helmet');
var mongoose = require('mongoose');
var cors = require("cors");
var bodyParser = require('body-parser');
var validate = require('express-jsonschema').validate;
var config = require('./config/config');
var errorHandler = require('./controllers/error/errorHandler');
var database = require('./controllers/database/database')
var votingRouter = require('./routes/voting');
var votingDBRouter = require('./routes/votingDB');

// App
const app = new express();
app.use(helmet());
app.use(helmet.xssFilter());
app.use(helmet.frameguard());

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))


// Routes
app.use('/'+API_VERSION, votingRouter);
app.use('/admin', votingDBRouter)

app.get('/api/version', (req, res) => {
    res.send('appVersion : ' + config.version);
});

app.on('ready', function() { 
    // listen port
    app.listen(PORT, HOST, () => {
        console.log("Server is up!");
        console.log(`Running on http://${HOST}:${PORT}`);
    });
});

// Error handling
app.use(errorHandler.clientErrorHandler);
app.use(errorHandler.handleError);

// connect db
console.log("Conencting Mongo " + database.connectHostURL);
database.connectHost()
.then((mongoose) => {
    console.log("Successful to connect MongoDB");
    app.emit('ready'); 
}).catch((err) => {
    console.log("MongoDB connection error");
    process.exit(1);
});


module.exports = app
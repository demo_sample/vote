'use strict';
const dotenv = require('dotenv');
dotenv.config();
 
const config = {
    version : '1.0.0',
    
     // Setting mongo host name
     'mongo_host': process.env.MONGO_HOST || 'localhost',
 
     // Setting mongo port number
     'mongo_port': process.env.MONGO_PORT || 27017,
 
     // Setting mongo database name
     'mongo_database': process.env.MONGO_DATABASE_NAME || 'defaultDB',
};
 
module.exports = config;

'use strict';

var VoteCampagin = require('../models/voteCampagin').MongooseModel;
var VoteOption = require('../models/voteOption').MongooseModel;
var UserVote = require('../models/userVote').MongooseModel;
var hkIDValidator = require ('./validation/HKIDValidator');
var errorHandler = require('./error/errorHandler');

// api for user voting
// curl sample
// curl -H "Content-Type: application/json" -d '{"hkID":"A123456(7)","voteCampaginNo":"1","voteOptionValue":"Michael Jordan"}' -X POST http://localhost:8080/v1/vote
module.exports.voting = async function(req, res){
    if(Object.keys(req.body).length === 0) {
        return errorHandler.send(res, 400, "Missing JSON string");
    }
    if(!req.body.hkID){
        return errorHandler.send(res, 400, "Missing HKID");
    }
    // hkid validation
    if(!hkIDValidator.IsHKID(req.body.hkID)){
        return errorHandler.send(res, 400, "invalid HKID");
    }
    
    if(!req.body.voteCampaginNo){
        return errorHandler.send(res, 400, "Missing voteCampaginNo");
    }  else if (!validArg(req.body.voteCampaginNo, "number")) {
        return errorHandler.send(res, 400, "invalid voteCampaginNo");
    }

    if(!req.body.voteOptionValue){
        return errorHandler.send(res, 400, "Missing voteOptionValue");
    } else if (!validArg(req.body.voteOptionValue)) {
        return errorHandler.send(res, 400, "invalid voteOptionValue");
    }

    var hkID = req.body.hkID;
    var voteCampaginNo = req.body.voteCampaginNo;
    var voteOptionValue = req.body.voteOptionValue;

    try {
        var voteCampagin = await VoteCampagin.findOne({ 
            "no": voteCampaginNo }).lean().exec();
        if(voteCampagin == null){
            return errorHandler.send(res, 400,  "voteCampaginNo:" + voteCampaginNo + " not found");
        }

        // check campagin in range
        if(Date.now() < voteCampagin.startDate || Date.now() > voteCampagin.endDate){
            return errorHandler.send(res, 400,  "voteCampaginNo:" + voteCampaginNo + " is end");
        }

        // check user is voted or not
        var userVote= await UserVote.findOne({ 
            "hkid": hkID,
            "voteCampagin": voteCampagin ,
        }).exec();

        if (userVote != null) {
            return errorHandler.send(res, 400,  "user hkid(" + hkID + ") is voted in campagin no("+voteCampaginNo+")");
        } 
            
        // check voteOption and update vote count 
        var voteOption = await VoteOption.findOne({ 
            "voteCampagin": voteCampagin , 
            "value" : voteOptionValue }).exec();
        
            
        if(voteOption == null){
            // create new vote option when not found
            var newVoteOption = 
                new VoteOption({
                    "voteCampagin"    : voteCampagin,
                    "value"           : voteOptionValue,
                    "voteCount"       : 1,
                });

            // save new vote option
            await newVoteOption.save();
            voteOption = newVoteOption;
            
        } else {
            // add vote count
            voteOption.voteCount ++;
            voteOption.save( function(err, savedVoteOption) {
                if (err) return errorHandler.send(res, 400, "Database Connection Error", err);
                voteOption = savedVoteOption;
            });
        }
        // save user vote
        var newUserVote = 
            new UserVote({
                "hkid"            : hkID,
                "voteCampagin"    : voteCampagin,
                "voteOption"      : voteOption,
            });
        
        await newUserVote.save(function(err, savedUserVote) {
            if (err) return errorHandler.send(res, 400, "Database Connection Error", err);
            return res.status(200).send("vote success");
        });
    } catch (error) {
        console.log(error);
        return errorHandler.send(res, 400,  "unexpected error" );
    }
    
};


module.exports.RequestJSONSchema = {
    type: 'object',
    properties: {
        hkID: {
            type: 'string',
            required: true
        },
        voteCampaginNo: {
            type: 'number',
            required: true
        },
        voteOptionValue: {
            type: 'string',
            required: true
        }
    }
}

function validArg(arg,  type = "string"){
    return !(arg == null || 
        arg == undefined || 
        arg == "" ||
        typeof arg != type);
}


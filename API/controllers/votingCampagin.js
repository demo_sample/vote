'use strict';

var VoteCampagin = require("../models/voteCampagin").MongooseModel;
var VoteOption = require("../models/voteOption").MongooseModel;
var UserVote = require("../models/userVote").MongooseModel;
var errorHandler = require('./error/errorHandler');
var _ = require('lodash');

module.exports.resetAllCampagin = function(req, res){
    try{
        var success = resetData();
        if(success){
            return res.send("Reset Success");
        }
    }catch(err){
        if (err) return errorHandler.send(res, 400, "Database Connection Error", err);
    }
};

async function resetData(){
    const voteCampagins = [
        new VoteCampagin({
            no: 1, 
            title : "Who is the best NBA player in the history",
            startDate:"2019-01-01 00:00:00",
            endDate:"2019-08-30 23:59:59",
        }),
        new VoteCampagin({
            no: 2, 
            title : "Which HK CEO candidate you are preferred.",
            startDate:"2019-01-01 00:00:00",
            endDate:"2019-08-30 23:59:59",
        })
    ];

    var query = await VoteCampagin.deleteMany({}, function(err){
        if(err) throw new Error("Database Connection Error");
    });

    query = await  VoteCampagin.insertMany(voteCampagins, function(err) {
        if(err) throw new Error("Database Connection Error");
    });

    const voteOptions = [
        {"voteCampagin" : voteCampagins[0], "value" : "Michael Jordan", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[0], "value" : "Kobe Bryant ", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[0], "value" : "Leborn James", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[0], "value" : "Stephen Curry", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[1], "value" : "Carrie Lam", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[1], "value" : "John Tsang", "voteCount" : 100},
        {"voteCampagin" : voteCampagins[1], "value" : "Rebecca Ip", "voteCount" : 100},
    ]
        
    query = await VoteOption.deleteMany({}, function(err){
        if(err) throw new Error("Database Connection Error");
    });

    query = await VoteOption.insertMany(voteOptions, function(err) {
        if(err) throw new Error("Database Connection Error");
    });

    query = await UserVote.deleteMany({}, function(err){
        if(err) throw new Error("Database Connection Error");
    });
    return true;
}


module.exports.GetCampaginDetail = function(req, res){

     var query = VoteCampagin.find().lean().exec(function(err, voteCampaginResult) {
        if (err) return errorHandler.send(res, 400, "Database Connection Error", err);
        
        var returnVoteCampagins = [];
        if (voteCampaginResult.length == 0) {
            // return res.send({"voteCampagin": returnVoteCampagins});
            resetData();
        }

        var voteOptions = {};
        // query vote option
        var query = VoteOption.find({
            "voteCampagin" : { "$in": _.map(voteCampaginResult, (result)=>{
                    return result._id;
                })
            }
        }).lean().exec(function(err, voteOptionResult) {
            _.map(voteOptionResult, (obj) => {
                if (!Array.isArray(voteOptions[obj.voteCampagin._id])) {
                    voteOptions[obj.voteCampagin._id] = [];
                }
                voteOptions[obj.voteCampagin._id].push({"value": obj.value, "voteCount": obj.voteCount});
            });
            
            // sort vote campagin by end date
            voteCampaginResult = _.values(voteCampaginResult).sort((a, b) => {
                var dateA = new Date(a.endDate); 
                var dateB = new Date(b.endDate);
                return dateA < dateB;})

            // check vote campagin 
            for (let p = 0; p < voteCampaginResult.length; p++) {
                var campagin = voteCampaginResult[p];
                if(Date.now() < campagin.startDate || Date.now() > campagin.endDate){
                    campagin.isAvailable = false; 
                } else {
                    campagin.isAvailable = true;
                }
                // sort vote count
                voteOptions[campagin._id] = _.values(voteOptions[campagin._id]).sort((a, b) => {
                    return a.voteCount < b.voteCount;})
                campagin.voteOptions = voteOptions[campagin._id];
                returnVoteCampagins.push(campagin);
            }
            return res.send({"voteCampagins": returnVoteCampagins});
        })
    })
};

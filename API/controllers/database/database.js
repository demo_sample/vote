'use strict';

const mongoose = require('mongoose');
const errorHandler = require('../error/errorHandler');
const config = require('../../config/config');

var connStr = 'mongodb://' + config.mongo_host;
connStr += ':' + config.mongo_port;
connStr += '/' + config.mongo_database;

module.exports.connectHost = function(){
    return mongoose.connect(this.connectHostURL, {useNewUrlParser: true})
}

module.exports.connectHostURL = connStr;

module.exports.handleError = function(err, res) {
    
    // handle dabaseError error
    console.error(err);

    res.send(err);
    
};
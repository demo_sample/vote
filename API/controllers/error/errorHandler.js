'use strict';


var send = function (res, errCode, errorMessage, err = null){
    if(err != null){
        console.error(err.stack);
    }
    var resJson = {};
    resJson.Code = errCode;
    resJson.Message = errorMessage;
    
    res.status(errCode).send(resJson);
}

var handleError = function (error, req, res, next) {
    // catch unexpected error
    send(res, 400, "Unknown error", error);
}

var clientErrorHandler = function (error, req, res, next) {
    if (error instanceof SyntaxError) {
        return send(res, 400, "Invalid JSON", error);
    } else if (error.name === 'JsonSchemaValidation') {
        return handleJsonSchemaValidation(error, req, res);
    } else {
        next(error);
    }
}

var handleJsonSchemaValidation = function (err, req, res){
    res.status(400);
    var responseData = {
        jsonSchemaValidation: true,
        validations: err.validations  
    };

    if (req.xhr || req.get('Content-Type') === 'application/json') {
        res.json(responseData);
    } 
}

module.exports = {
    send: send,
    handleError: handleError,
    clientErrorHandler: clientErrorHandler,
    handleJsonSchemaValidation: handleJsonSchemaValidation,
  }
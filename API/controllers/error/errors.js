class GeneralError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
    }
}

class ArgumentsError extends GeneralError {
    constructor(message) {
        super(message);
        this.code = 501;
    }
}

class InternalError extends GeneralError {
    constructor(error = null) {
        if(error == null){
            super("Unknown error");
        } else {
            super(error.message);
        }
        this.data = {error};
        this.code = 500;
    }
}

module.exports = {
    InternalError,  
    ArgumentsError,
};

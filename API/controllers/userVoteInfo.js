'use strict';

var UserVote = require('../models/userVote').MongooseModel;
var errorHandler = require('./error/errorHandler');
var hkIDValidator = require ('./validation/HKIDValidator');

// api for user voting
// curl sample
// curl -H "Content-Type: application/json" -d '{"hkID":"A123456(7)"}' -X POST http://localhost:8080/v1/userVoteDetail
module.exports.userVoteInfo = async function(req, res){
    if(Object.keys(req.body).length === 0) {
        return errorHandler.send(res, 400, "Missing JSON string");
    }
    if(!req.body.hkID){
        return errorHandler.send(res, 400, "Missing HKID");
    }
    // hkid validation
    if(!hkIDValidator.IsHKID(req.body.hkID)){
        // return errorHandler.send(res, 400, "invalid HKID");
        
    }

    var hkid = req.body.hkID;
    try{
        var userVotes = await UserVote.find({
            "hkid" : hkid,
        }).populate("voteOption").lean().exec();
        
        // prepare response
        var returnMessage = {"hkid" : hkid, userVotes:[]};
        for(var i = 0; i < userVotes.length; i++){
            
            var returnVote = {
                "_id" : userVotes[i]._id,
                "voteCampagin" : userVotes[i].voteCampagin,
                "voteOptionValue" : userVotes[i].voteOption.value,

            }
            returnMessage.userVotes.push(returnVote);
        }
        return res.send(returnMessage);
    }catch(error){
        console.log(error);
        return errorHandler.send(res, 400,  "unexpected error" );
    }
    
};

module.exports.RequestJSONSchema = {
    type: 'object',
    properties: {
        hkID: {
            type: 'string',
            required: true
        }
    }
}










## Voting system
#### Pre-requisite
- **node version 10.16.2**
install via nvm or from nodejs web site
-  **install mongoDB v4.0.12**
install via https://www.mongodb.com/download-center#community

### Backend server
using express + mongoose

#### Installation and start
```
# go to working directory
cd API

# npm install 
npm install

# run in local machine
# default host: http://localhost:8080 (edit port in "app.js")
npm run start
```

### Frontend server
using reactjs + axios

#### Installation and start
```
# go to working directory
cd client

# npm install 
npm install

# run in local machine
# default host: http://localhost:3000 (edit port in "Dockerfile")
# Main Page Url: http://localhost:3000
npm run start
```

#### Database
using Mongo

``` bash
# for localhost 
mongod --port 27018
```


###  Run via Docker
``` bash
# start frontend, backend, mongo by docker images
sh start_server.sh
```
#### Default host
host: 127.0.0.1
#### Port configuration
modify the port in docker-compose.yml for different service

**default port in docker:**
- frontend : 9000 
- backend : 49160 
- database : 27017

#### Remark 
**Useful API**
1) **GET**  get api version
**Request**
``` bash
#Curl Sample
curl -H "Content-Type: application/json" http://localhost:8080/api/version
```
**Response**
```
appVersion : 1.0.0
```

2) **GET**  - get vote campaign detail
**Request**
``` bash
#Curl Sample
curl -H "Content-Type: application/json" http://localhost:8080/v1/voteDetail
```
**Response**
Data type: json
```
{
   "voteCampagins":[
      {
         "_id":"5d584fbd59d75e3510df7aa9",
         "no":2,
         "title":"Which HK CEO candidate you are preferred.",
         "startDate":"2018-12-31T16:00:00.000Z",
         "endDate":"2019-08-30T15:59:59.000Z",
         "__v":0,
         "isAvailable":true,
         "voteOptions":[
            {
               "value":"Carrie Lam",
               "voteCount":100
            },
            {
               "value":"John Tsang",
               "voteCount":100
            },
            {
               "value":"Rebecca Ip",
               "voteCount":100
            }
         ]
      }
      ...
   ]
}
```
3) **POST** - get user vote detail
**Request**
Data type: json
Parameter: "hkID" : string
``` bash
#Curl Sample
curl -H "Content-Type: application/json" -d '{"hkID":"1"}' -X POST http://localhost:8080/v1/userVoteDetail
```
**Response**
Data type: json
``` json
{
   "hkid":"1",
   "userVotes":[
      {
         "_id":"5d59644cf67f983b05a168e8",
         "voteCampagin":"5d584fbd59d75e3510df7aa8",
         "voteOptionValue":"2"
      }
   ]
}
```

4) **POST** - user vote
**Request**
Data type: json
Parameter: "hkID" : string
Parameter: "voteCampaginNo" : number
Parameter: "voteOptionValue" : string
``` bash
#Curl Sample
url -i -H "Content-Type: application/json" -d '{"hkID":"A123456(7)","voteCampaginNo":1,"voteOptionValue":"Michael Jordan"}' -X POST http://localhost:8080/v1/vote
```
**Response**
Data type: string
```
# successful response - 200 status
vote success
```

5) **GET**  - reset all campaign data
**Request**
``` bash
#Curl Sample
curl -H "Content-Type: application/json" http://localhost:8080/admin/resetVoteCampagin
```
**Response**
Data type: string
```
# successful response - 200 status
Reset Success
```